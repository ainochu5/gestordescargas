package com.ainochu.descargaficheros.gui;

import com.ainochu.descargaficheros.gui.SwingWorker.Tarea;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by Ainoa on 11/11/2015.
 */
public class Descarga implements ActionListener {
    private JProgressBar pbProgresoDescarga;
    JPanel panel1;
    private JButton btBorrar;
    private JButton btIniciar;
    private JLabel lbNombreDescarga;
    private JLabel lbProgresoDescarga;
    private JButton btReiniciarDescarga;
    private JLabel lbtotalDescarga;
    private JLabel lbPorcentaje;
    private boolean activado;
    private JTabbedPane fichas;
    private ArrayList<Descarga>listaD;
    private Tarea tarea;
    private int cont;
    private int contDescargas;
    private JPanel panel;
    private GestorDescargas gestor;
    private boolean extra;


    public Descarga(GestorDescargas gestor,JTabbedPane fichas, ArrayList<Descarga>listaD, int cont) {
        panel1.setSize(200, 200);
        btBorrar.setActionCommand("Borrar");
        btBorrar.addActionListener(this);
        btIniciar.setActionCommand("Cancelar");
        btIniciar.addActionListener(this);
        btReiniciarDescarga.setActionCommand("Reiniciar");
        btReiniciarDescarga.addActionListener(this);
        this.fichas = fichas;
        this.listaD = listaD;
        this.cont = cont;
        this.gestor = gestor;
    }


    public void iniciar(String nombreDescarga){
        lbNombreDescarga.setText(nombreDescarga);
        tarea = new Tarea(nombreDescarga,cont);
        //contDescargas = cont - contDescargas;
        tarea.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("progress")) {
                    pbProgresoDescarga.setValue((Integer) evt.getNewValue());
                    lbPorcentaje.setText(evt.getNewValue().toString()+ "%");
                }
                if (evt.getPropertyName().equals("total")) {
                    lbtotalDescarga.setText(" de " + evt.getNewValue());
                }
                if (evt.getPropertyName().equals("descargado")) {
                    lbProgresoDescarga.setText("Descargado: " + evt.getNewValue());
                }
                if (evt.getPropertyName().equals("completado")) {
                        try {
                            gestor.cargarSiguienteDescarga(listaD,extra);
                        } catch (IOException e) {
                            e.printStackTrace();
                    }
                }
                if (evt.getPropertyName().equals("cancelada")) {
                    borrarDescarga();
                }

            }
        });
        tarea.execute();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Borrar":
                tarea.cancel(true);
                break;
            case "Cancelar":
                tarea.cancel(true);
                break;
            case "Reiniciar":
                reiniciarDescarga();
                break;
        }
    }

    public void borrarDescarga(){
        activado = true;
        fichas.remove(fichas.getSelectedIndex());
        listaD.remove(fichas.getSelectedIndex());
    }


    public void reiniciarDescarga(){
        pbProgresoDescarga.setValue(0);
        lbProgresoDescarga.setText("0");
        tarea.execute();
        lbtotalDescarga.setText("0");
        lbPorcentaje.setText("0%");
    }

}
