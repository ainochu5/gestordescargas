package com.ainochu.descargaficheros.gui.util;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import static com.ainochu.descargaficheros.gui.util.Constantes.RUTAALTERNATIVA;
import static com.ainochu.descargaficheros.gui.util.Constantes.RUTADESCARGAS;
import static com.ainochu.descargaficheros.gui.util.Constantes.RUTAHISTORIAL;

/**
 * Created by Ainoa on 19/11/2015.
 */
public class Util extends Component {

    public static int cantidad = 2;
    private Properties propiedades;

    public void guardarConfiguracion(boolean nuevaDescarga){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if(fileChooser.showSaveDialog(null)==fileChooser.APPROVE_OPTION) {
            File archivo = fileChooser.getSelectedFile();
            RUTADESCARGAS = archivo.getAbsolutePath();
            RUTAALTERNATIVA = archivo.getAbsolutePath();
        }
        if(nuevaDescarga == true) {
            propiedades = new Properties();
            propiedades.setProperty("ruta descargas:", RUTAALTERNATIVA);
        }
        else {
            String respuesta = JOptionPane.showInputDialog(null, "Introduzca numero de descargas");
            cantidad = Integer.parseInt(respuesta);
            propiedades = new Properties();
            propiedades.setProperty("ruta descargas:", RUTADESCARGAS);
        }
        propiedades.setProperty("Descargas simultaneas:", Integer.toString(cantidad));
        try {
            propiedades.store(new FileOutputStream("archivo.conf"),"archivo de configuracion");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void abrirArchivoConfiguracion(){
        File f = new File("archivo.conf");
        if(f.exists()) {
            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream("archivo.conf"));
                RUTADESCARGAS = properties.getProperty("ruta descargas:");
                cantidad = Integer.parseInt(properties.getProperty("Descargas simultaneas:"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void rellenarHistorialDescargas(boolean completado, String nombreDescarga){
        //ABRO EL ARCHIVO DE HISTORIAL DE DESCARGAS
        try {
            //OBTENER LA HORA DEL SISTEMA
            Calendar calendario = new GregorianCalendar();
            int hora, minutos, segundos;
            hora =calendario.get(Calendar.HOUR_OF_DAY);
            minutos = calendario.get(Calendar.MINUTE);
            segundos = calendario.get(Calendar.SECOND);
            String fecha = hora + ":" + minutos + ":" + segundos + " ";
            FileWriter fichero = new FileWriter(RUTAHISTORIAL,true);
            PrintWriter pw = new PrintWriter(fichero);
            if(completado == true) {
                pw.println(fecha + nombreDescarga + " Completado");
            }
            else{
                pw.println(fecha + nombreDescarga + " Fallido");
            }
            fichero.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
