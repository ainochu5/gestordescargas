package com.ainochu.descargaficheros.gui.util;

import java.io.File;

/**
 * Created by Ainoa on 19/11/2015.
 */
public class Constantes {
    public static String RUTADESCARGAS = System.getProperty("user.home")+ File.separator + "Desktop";
    public static String RUTAHISTORIAL = System.getProperty("user.home")+File.separator+"historial.txt";
    public static String RUTAALTERNATIVA;
}
