package com.ainochu.descargaficheros.gui.SwingWorker;

import com.ainochu.descargaficheros.gui.util.Util;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import static com.ainochu.descargaficheros.gui.util.Constantes.RUTADESCARGAS;

/**
 * Created by Ainoa on 17/11/2015.
 */
public class Tarea extends SwingWorker<Void, Integer> {

    private String nombre;;
    private boolean completado;
    private int cont;
    private String nombreArchivo;
    private FileOutputStream fos;
    private int contadorDescargas;

    public Tarea(String nombre_descarga, int cont){
        this.nombre = nombre_descarga;
        this.cont = cont;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Util util = new Util();
        try {
            // Url con la foto
            URL url = new URL(nombre);

            // establecemos conexion
            URLConnection urlCon = url.openConnection();
            // Sacamos por pantalla el tipo de fichero
            String tipoArchivo = urlCon.getContentType().substring(urlCon.getContentType().lastIndexOf('/') + 1, urlCon.getContentType().length());
            String urls[] = tipoArchivo.split("\"");
            //Obtener el archivo, su tamaño y abrirlo
            InputStream is = urlCon.getInputStream();
            int tamanoFichero = urlCon.getContentLength();
            nombreArchivo = RUTADESCARGAS+File.separator+urls[1];
            fos = new FileOutputStream(nombreArchivo);

            // Lectura del web y escritura en fichero local
            byte[] bytes = new byte[2048];
            int longitud = 0;
            int progresoDescarga = 0;

            // Descarga el fichero y va notificando el progreso
            while ((longitud = is.read(bytes)) != -1) {
                fos.write(bytes, 0, longitud);

                progresoDescarga += longitud;
                setProgress((int) (progresoDescarga * 100 / tamanoFichero));
                if(isCancelled()){
                    util.rellenarHistorialDescargas(completado, nombre);
                    int respuesta = JOptionPane.showConfirmDialog(null,"�Desea borrar el archivo?");
                    if(respuesta == JOptionPane.YES_OPTION){
                        fos.close();
                        File fichero = new File(nombreArchivo);
                        fichero.delete();
                        JOptionPane.showMessageDialog(null,"El archivo se borro correctamente.");
                        firePropertyChange("cancelada", null, "Cancelado");
                    }
                    if(respuesta == JOptionPane.CANCEL_OPTION){
                        JOptionPane.showMessageDialog(null,"El archivo no ha sido borrado.");
                    }
                    cancel(false);
                    break;
                }
                //CON ESTO NOTIFICAMOS EL CAMBIO
                firePropertyChange("descarga", null, nombre);
                firePropertyChange("descargado",null,progresoDescarga);
                firePropertyChange("total", null, tamanoFichero);

            }
            if(isCancelled()!=true) {
                setProgress(100);
                completado = true;
                if (cont > 0) {
                    firePropertyChange("completado", null, completado);
                }
                //LLAMAMOS AL METODO QUE GUARDA EL NOMBRE DEL ARCHIVO
                util.rellenarHistorialDescargas(completado, nombre);
            }
            // cierre de conexion y fichero.
            is.close();
            fos.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error con la url");
            util.rellenarHistorialDescargas(completado, nombre);
        }
        return null;
    }

}
