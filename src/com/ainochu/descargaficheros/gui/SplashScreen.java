package com.ainochu.descargaficheros.gui;

import javax.swing.*;

/**
 * Created by Ainoa on 20/11/2015.
 */
public class SplashScreen extends Thread{
    private JPanel panel1;
    private JFrame frame;

    public SplashScreen(){
        frame = new JFrame();
        frame.getContentPane().add(panel1);
        frame.setUndecorated(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    public void cargar(boolean estado){
        frame.setVisible(estado);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        frame.setVisible(false);
    }
}
