package com.ainochu.descargaficheros.gui;

import com.ainochu.descargaficheros.gui.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

import static com.ainochu.descargaficheros.gui.util.Constantes.RUTAHISTORIAL;
import static com.ainochu.descargaficheros.gui.util.Util.cantidad;

/**
 * Created by Ainoa on 13/11/2015.
 */
public class GestorDescargas implements ActionListener {
    private JPanel panel;
    private JTabbedPane tabbedPane1;
    private JTextField textUrlDescarga;
    private JButton btIniciar;
    private JButton btAnadir;
    private JPanel jpPrincipal;
    private JList ltListaDescargas;
    private JPanel jpDescargas;
    private ArrayList<Descarga>listaDescargas;
    private int contDescargas = 1;
    private int cont;
    private int contadorLineas;
    private String lineaURL;
    private DefaultListModel modelo;
    private boolean extra;
    private boolean nuevaDescarga;
    private JMenuItem item2;

    private void cargarListeners(){
        btAnadir.addActionListener(this);
        btAnadir.setActionCommand("anadir");

    }
    public GestorDescargas(){
        Util util = new Util();
        util.abrirArchivoConfiguracion();
        SplashScreen splashScreen = new SplashScreen();
        splashScreen.start();
        splashScreen.cargar(true);
        crearVentana();
        listaDescargas = new ArrayList<>();
        modelo = new DefaultListModel<>();
        ltListaDescargas.setModel(modelo);
        cargarListeners();
    }

    public void crearVentana(){
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel);
        frame.setLocationRelativeTo(null);
        cambiarApariencia();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(550,350);
        frame.setJMenuBar(crearMenuBar());
        frame.setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "anadir":
                System.out.println(contDescargas);
                if(contDescargas > cantidad) {
                        extra = true;
                        anadirURLArchivo();
                    return;
                }
                nuevaDescarga = true;
                configuracionInicialDescarga();
                anadirNuevaDescarga();
                nuevaDescarga = false;
                break;
            default:
                break;
        }
    }

    public void anadirNuevaDescarga(){
        Descarga panelDescarga = new Descarga(this, tabbedPane1, listaDescargas, cont);
        tabbedPane1.add(panelDescarga.panel1, "Nueva Descarga");
        panel.revalidate();
        listaDescargas.add(panelDescarga);
        if(extra == false) {
            panelDescarga.iniciar(textUrlDescarga.getText());
        }
        else{
            panelDescarga.iniciar(lineaURL);
        }
        contDescargas++;
        cont++;
        textUrlDescarga.setText("");
    }
    public JMenuBar crearMenuBar(){
        JMenuBar menu = new JMenuBar();
        JMenu menu1=new JMenu("Opciones");
        JMenu menu2=new JMenu("Descargas");
        menu.add(menu1);
        menu.add(menu2);
        JMenuItem item1=new JMenuItem("Ruta descarga");
        item2=new JMenuItem("Historial descargas");
        menu1.add(item1);
        menu2.add(item2);
        item1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Util util = new Util();
                util.guardarConfiguracion(nuevaDescarga);
            }
        });
        item2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //ruta del archivo en el pc
                String file = new String(RUTAHISTORIAL);

                try{
                    File f = new File(file);
                    if (f.exists()) {
                        //definiendo la ruta en la propiedad file
                        Runtime.getRuntime().exec("cmd /c start " + file);
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Todavía no se ha descargado nada");
                    }

                }catch(IOException ev){
                    ev.printStackTrace();
                }
            }
        });
        return menu;
    }

    public void configuracionInicialDescarga(){
        int reply = JOptionPane.showConfirmDialog(null, "¿Desea cambiar la ruta de la descarga?","Cambiar configuracion", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION)
        {
            Util util = new Util();
            nuevaDescarga = true;
            util.guardarConfiguracion(nuevaDescarga);
            nuevaDescarga = false;
        }
    }


    public void cargarSiguienteDescarga(ArrayList<Descarga> listaDescargas, boolean extra) throws IOException {
        this.extra = true;
        ArrayList<String>listaURLS = new ArrayList<>();
        modelo.removeAllElements();
        BufferedReader lector = new BufferedReader(new FileReader("ListaDescargas.txt"));
        String linea;
        while((linea = lector.readLine()) != null){
            if(contadorLineas!=0){
                listaURLS.add(linea);
            }
            else{
                lineaURL = linea;
            }
            contadorLineas++;
        }
        if (contadorLineas == 0){
            JOptionPane.showMessageDialog(null,"No quedan descargas en cola");
            return;
        }
        lector.close();
        FileWriter fichero = null;
        fichero = new FileWriter("ListaDescargas.txt");
        PrintWriter pw = new PrintWriter(fichero);
        for(int i = 0; i < listaURLS.size(); i++){
            pw.println(listaURLS.get(i));
            modelo.addElement(listaURLS.get(i));
        }
        pw.close();
        anadirNuevaDescarga();
        ltListaDescargas.setModel(modelo);
        contadorLineas = 0;
    }

    private static void cambiarApariencia(){
        try{
            for (javax.swing.UIManager.LookAndFeelInfo info: javax.swing.UIManager.getInstalledLookAndFeels()){
                if ("Nimbus".equals(info.getName())){
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }catch (Exception e){
            System.out.print(e);
        }
    }

    public void anadirURLArchivo(){
        FileWriter fichero = null;
        try {
            fichero = new FileWriter("ListaDescargas.txt",true);
            PrintWriter pw = new PrintWriter(fichero);
            lineaURL = textUrlDescarga.getText();
            if(textUrlDescarga.getText().isEmpty()){
                return;
            }
            modelo.addElement(lineaURL);
            pw.println(lineaURL);
            pw.close();
            ltListaDescargas.setModel(modelo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
